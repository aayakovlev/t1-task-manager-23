package ru.t1.aayakovlev.tm.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.exception.field.AbstractFieldException;
import ru.t1.aayakovlev.tm.model.Project;

public interface ProjectService extends UserOwnedService<Project> {

    @NotNull
    Project create(@Nullable final String userId, @Nullable final String name) throws AbstractFieldException;

    @NotNull
    Project create(
            @Nullable final String userId,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractFieldException;

}
