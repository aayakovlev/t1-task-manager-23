package ru.t1.aayakovlev.tm.service.impl;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;
import ru.t1.aayakovlev.tm.exception.auth.AccessDeniedException;
import ru.t1.aayakovlev.tm.exception.auth.FailedLoginException;
import ru.t1.aayakovlev.tm.exception.auth.PasswordIncorrectException;
import ru.t1.aayakovlev.tm.exception.auth.UserNotLoggedException;
import ru.t1.aayakovlev.tm.exception.entity.EntityNotFoundException;
import ru.t1.aayakovlev.tm.exception.field.LoginEmptyException;
import ru.t1.aayakovlev.tm.exception.field.PasswordEmptyException;
import ru.t1.aayakovlev.tm.model.User;
import ru.t1.aayakovlev.tm.service.AuthService;
import ru.t1.aayakovlev.tm.service.UserService;
import ru.t1.aayakovlev.tm.util.HashUtil;

import java.util.Arrays;

public final class AuthServiceImpl implements AuthService {

    @NotNull
    private final UserService userService;

    @Nullable
    private String userId;

    public AuthServiceImpl(@NotNull final UserService userService) {
        this.userService = userService;
    }

    @Override
    public void checkRoles(@Nullable final Role[] roles) throws AbstractException {
        if (roles == null) return;
        @NotNull final User user = getUser();
        @NotNull final Role role = user.getRole();
        final boolean hasRole = Arrays.asList(roles).contains(role);
        if (!hasRole) throw new AccessDeniedException();
    }

    @Override
    @NotNull
    public User getUser() throws AbstractException {
        if (!isAuth()) throw new UserNotLoggedException();
        @Nullable final User user = userService.findById(userId);
        if (user == null) throw new EntityNotFoundException();
        return user;
    }

    @Override
    @NotNull
    public String getUserId() throws UserNotLoggedException {
        if (!isAuth()) throw new UserNotLoggedException();
        return userId;
    }

    @Override
    public boolean isAuth() {
        return userId != null;
    }

    @Override
    public void login(@Nullable final String login, @Nullable final String password) throws AbstractException {
        if (login == null || login.isEmpty()) throw new LoginEmptyException();
        if (password == null || password.isEmpty()) throw new PasswordEmptyException();
        @Nullable final User user = userService.findByLogin(login);
        if (user == null) throw new FailedLoginException();
        final boolean locked = user.isLocked();
        if (locked) throw new FailedLoginException();
        @NotNull final String hash = HashUtil.salt(password);
        if (!hash.equals(user.getPasswordHash())) throw new PasswordIncorrectException();
        userId = user.getId();
    }

    @Override
    public void logout() {
        userId = null;
    }

    @Override
    @NotNull
    public User registry(
            @Nullable final String login,
            @Nullable final String password,
            @Nullable final String email
    ) throws AbstractException {
        return userService.create(login, password, email);
    }

}
