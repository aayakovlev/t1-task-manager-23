package ru.t1.aayakovlev.tm.command.user;

import org.jetbrains.annotations.NotNull;
import ru.t1.aayakovlev.tm.enumerated.Role;
import ru.t1.aayakovlev.tm.exception.AbstractException;

import static ru.t1.aayakovlev.tm.util.TerminalUtil.nextLine;

public final class UserUpdateProfileCommand extends AbstractUserCommand {

    @NotNull
    public static final String DESCRIPTION = "Update user profile.";

    @NotNull
    public static final String NAME = "user-update-profile";

    @Override
    @NotNull
    public String getDescription() {
        return DESCRIPTION;
    }

    @Override
    @NotNull
    public String getName() {
        return NAME;
    }

    @Override
    @NotNull
    public Role[] getRoles() {
        return Role.values();
    }

    @Override
    public void execute() throws AbstractException {
        System.out.println("[USER UPDATE PROFILE]");
        System.out.print("Enter first name: ");
        @NotNull final String firstName = nextLine();
        System.out.print("Enter last name: ");
        @NotNull final String lastName = nextLine();
        System.out.print("Enter middle name: ");
        @NotNull final String middleName = nextLine();
        @NotNull final String userId = getUserId();
        getUserService().updateUser(userId, firstName, lastName, middleName);
    }

}
